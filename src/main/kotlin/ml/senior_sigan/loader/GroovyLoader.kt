package ml.senior_sigan.loader

import groovy.lang.GroovyClassLoader
import groovy.lang.GroovyShell
import groovy.lang.Script
import org.slf4j.LoggerFactory
import java.io.IOException
import java.lang.reflect.Proxy
import java.nio.file.Paths

class GroovyLoader(val scriptsPath: String) : IScriptLoader {
    private val log = LoggerFactory.getLogger(javaClass)

    @Suppress("UNCHECKED_CAST")
    override fun <T> load(name: String, clazz: Class<T>): T {
        val classLoader = GroovyClassLoader(javaClass.classLoader)
        val shell = GroovyShell()
        val scriptPath = Paths.get(scriptsPath, name + ".groovy")

        try {
            val script = shell.evaluate(scriptPath.toFile()) as Script
            val obj = Proxy.newProxyInstance(classLoader, arrayOf(clazz)) { _, method, args ->
                script.invokeMethod(method?.name, args)
            }
            log.info("Found script $scriptPath")
            return obj as T
        } catch (e: IOException) {
            throw LoaderException("Script not found: $scriptsPath", e)
        } catch (e: Exception) {
            throw LoaderException("Could not load groovy script", e)
        }
    }
}