package ml.senior_sigan.loader

interface IOperation<in L, in R, out T> {
    val name: String
    fun execute(left: L, right: R): T
}