package ml.senior_sigan.loader

interface IScriptLoader {
    fun <T> load(name: String, clazz: Class<T>): T
}