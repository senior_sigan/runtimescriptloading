package ml.senior_sigan.loader

import org.slf4j.LoggerFactory
import java.io.File
import java.net.URLClassLoader
import javax.tools.ToolProvider


class JavaLoader(private val scriptsPath: String, private val packageName: String) : IScriptLoader {
    private val log = LoggerFactory.getLogger(javaClass)

    @Suppress("UNCHECKED_CAST")
    private fun <T> load(name: String): T {
        try {
            val root = File(scriptsPath)
            val script = File(root, packageName.replace(".", "/") + name + ".java")
            val compiler = ToolProvider.getSystemJavaCompiler()
            val error = compiler.run(null, null, null, script.path)
            if (error != 0) throw Exception("Compilation failed with error code $error")
            val classLoader = URLClassLoader.newInstance(arrayOf(root.toURI().toURL()))
            val cls = Class.forName(packageName + name, true, classLoader)
            val instance = cls.newInstance()
            log.info("Found script " + script.absolutePath)
            return instance as T
        } catch (e: Exception) {
            throw LoaderException("Can't load java class by name " + name, e)
        }

    }

    override fun <T> load(name: String, clazz: Class<T>): T {
        return load(name)
    }
}