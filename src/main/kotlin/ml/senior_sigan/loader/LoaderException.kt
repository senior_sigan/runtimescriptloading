package ml.senior_sigan.loader

class LoaderException(message: String?, cause: Throwable?) : Exception(message, cause)