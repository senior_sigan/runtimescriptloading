package ml.senior_sigan.loader

import org.junit.Assert
import org.junit.Test
import java.io.File

class GroovyLoaderTest {
    @Test
    fun shouldLoadClass() {
        val loader: IScriptLoader = GroovyLoader(File("src/test/resources").absolutePath)
        val op = loader.load("AdditionOperation", IOperation::class.java) as IOperation<Int, Int, Int>
        Assert.assertEquals(4, op.execute(2, 2))
        Assert.assertEquals(5, op.execute(2, 3))
        Assert.assertEquals(-3, op.execute(2, -5))
    }
}