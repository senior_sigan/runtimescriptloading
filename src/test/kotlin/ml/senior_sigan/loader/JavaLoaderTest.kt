package ml.senior_sigan.loader

import org.junit.Assert
import org.junit.Test
import java.io.File

class JavaLoaderTest {
    @Test
    fun shouldLoadClass() {
        val loader: IScriptLoader = JavaLoader(File("src/test/resources").absolutePath, "operations.")
        val op = loader.load("MultiplicationOperation", IOperation::class.java) as IOperation<Int, Int, Int>
        Assert.assertEquals(4, op.execute(2, 2))
        Assert.assertEquals(6, op.execute(2, 3))
        Assert.assertEquals(-10, op.execute(2, -5))
    }
}