package operations;

import org.jetbrains.annotations.NotNull;

import ml.senior_sigan.loader.IOperation;

public class MultiplicationOperation implements IOperation<Integer, Integer, Integer> {

    @NotNull
    @Override
    public String getName() {
        return "multiplication";
    }

    @Override
    public Integer execute(final Integer left, final Integer right) {
        return left * right;
    }
}

